package com.dr.code.diff.config;

import com.dr.code.diff.CodeDiffApplicationTest;
import com.dr.code.diff.dto.DiffMethodParams;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.CompareResults;
import org.gitlab4j.api.models.Project;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ProjectName: base-service
 * @Package: com.dr.codediff.config
 * @Description: java类作用描述
 * @Author: duanrui
 * @CreateDate: 2021/1/7 19:56
 * @Version: 1.0
 * <p>
 * Copyright: Copyright (c) 2021
 */
class GitConfigTest extends CodeDiffApplicationTest {

//    @Autowired
//    private GitConfig gitConfig;

//    @Test
//    void cloneRepository() {
//        String localRepo = "D:\\git-test\\base-service";
//        String gitUrl = "http://192.168.75.128/rayduan/base-service.git";
//        gitConfig.cloneRepository(gitUrl, localRepo, "2ea401406d775005245faa0a57d6e08db348433f");
//    }
//
//    @Test
//    void diffMethods() {
////        DiffMethodParams diff = DiffMethodParams.builder()
////                .baseVersion("dac68b2f3976509b4204a66df8f5e71dffe023b7")
////                .nowVersion("74c026849763f1f9f0dfc967949d0b581959c2ea")
////                .gitUrl("https://github.com/rayduan/devops-data.git")
////                .build();
//        DiffMethodParams diff = DiffMethodParams.builder()
//                .baseVersion("db84201eea9be107073fa07c24e30262ac91d384")
//                .nowVersion("dca3a374ba9100c64b970081fad5db7335e99ff6")
//                .gitUrl("https://github.com/rayduan/base-service.git")
//                .build();
//        gitConfig.diffMethods(diff);
//    }

    @Test
    void testRebase(){
        String gitLabUrl = "http://127.0.0.1:9080/";
        String privateToken = "glpat-3232323";

        // Project ID or path
        String projectIdOrPath = "dray/jacoco-demo";

        // Branches to compare
        String fromBranch = "master";
        String toBranch = "feature";

        // Create a GitLabApi instance
        GitLabApi gitLabApi = new GitLabApi(gitLabUrl, privateToken);

        try {
            // Compare the branches
            CompareResults compareResults = gitLabApi.getRepositoryApi().compare(projectIdOrPath, fromBranch, toBranch);

            // Print the results
            compareResults.getCommits().forEach(commit -> {
                System.out.println("Commit: " + commit.getTitle());
            });
            compareResults.getDiffs().forEach(diff -> {
                System.out.println("Diff: " + diff.getNewPath());
            });
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }

    }
}